---
layout: page
title: "Title"
author: Name
language: en
summary: "Description of the symptom"
date: 20XX-XX
permalink: /en/topics/workflow_template
parent: Home
---

# Title

[Introduction: one or two paragraphs that explain what the issue might be about.]

Here is a questionnaire to identify the nature of your problem and find possible solutions.

## Workflow

### [urgent_question_1]

Do you [...]?

 - [Yes](#[services]_end) [referral to organizations that can help mitigate the emergency]
 - [No](#second_question) [If there is no life-threatening situation, proceed with diagnostic process]

### [second_question]

Question to start the diagnostic process, having ruled out any emergency that might be life-threatening.

 - [Yes](#third_question)
 - [No](#fourth_question)

### third_question

> Recommendation
>
> Instructions to implement the recommendation

[Ask if the recommended measure has worked]

 - [Yes](#final-tips)
 - [No](#etc)

### fourth_question

> Explanation of what might be happening
>
> More details on what might be happening

[Question to figure out if this is the problem]

 - [Yes](link to one more recommendation or to [serviceA]_end)
 - [No](#etc)

### etc

> More recommendations
>
> How to implement them

[Ask if the recommended measure has worked]

 - [Yes](#resolved_end)
 - [No](#[serviceB]_end)


### [serviceA]_end

> If you are still experiencing [...], please contact the organizations below who can support you.

:[](organisations?services=serviceA&serviceB) [replace "serviceA" and "serviceB" in the link with one of the services included in the "services" meta tags in the "Organizations" files.]

### [serviceB]_end

> If you are still experiencing [...], please contact the organizations below who can support you.

:[](organisations?services=serviceA&serviceB) [replace "serviceA" and "serviceB" in the link with one of the services included in the "services" meta tags in the "Organizations" files.]

### resolved_end

We're really glad that your problem is solved!

### final_tips

[a series of tips to mitigate the risk of this issue arising again]

#### resources

[a series of links to learn more about the topic]
