---
layout: page
title: Email
author: mfc
language: de
summary: Contact methods
date: 2018-09
permalink: /de/contact-methods/email.md
parent: /de/
published: true
---

The content of your message as well as the fact that you contacted the organization may be accessible by governments or law enforcement agencies.