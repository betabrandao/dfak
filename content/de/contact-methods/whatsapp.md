---
layout: page
title: WhatsApp
author: mfc
language: de
summary: Contact methods
date: 2018-09
permalink: /de/contact-methods/whatsapp.md
parent: /de/
published: true
---

Using WhatsApp will ensure your conversation with the recipient is protected so that only you and the recipient can read the communications, however the fact that you communicated with the recipient may be accessible by governments or law enforcement agenices.

Resources: [How to: Use WhatsApp on Android](https://ssd.eff.org/en/module/how-use-whatsapp-android) [How to: Use WhatsApp on iOS](https://ssd.eff.org/en/module/how-use-whatsapp-ios).
