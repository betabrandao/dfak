---
layout: page.pug
title: "About"
language: de
summary: "About the Digital First Aid Kit."
date: 2019-03-13
permalink: /de/about/
parent: Home
---

The Digital First Aid Kit is a collaborative effort of the [RaReNet (Rapid Response Network)](https://www.rarenet.org/) and [CiviCERT](https://www.civicert.org/).

The Rapid Response Network is an international network of rapid responders and digital security champions which includes the EFF, Global Voices, Hivos & the Digital Defenders Partnership, Front Line Defenders, Internews, Freedom House, Access Now, Virtual Road, CIRCL, Open Technology Fund, as well as individual security experts who are working in the field of digital security and rapid response.

Some of these organisations and individuals are part of CiviCERT, an international network of digital security help desks and infrastructure providers that are mainly focused on supporting groups and organizations striving towards social justice and the defense of human and digital rights. CiviCERT is a professional framing for the rapid response community’s distributed CERT (Computer Emergency Response Team) efforts. CiviCERT is accredited by Trusted Introducer, the European network of trusted computer emergency response teams.

The Digital First Aid Kit is also an [open-source project that accepts outside contributions.](https://gitlab.com/rarenet/dfak)