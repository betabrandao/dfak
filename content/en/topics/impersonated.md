---
layout: page
title: "Someone is Impersonating Me Online"
author: Floriana Pagano, Alexandra Hache
language: en
summary: "Someone is being impersonated through a social media account, email address, PGP key, fake website or app"
date: 2019-04-01
permalink: /en/topics/impersonated
parent: Home
---

# Someone is Impersonating Me Online

A threat faced by many activists, Human Rights Defenders, NGOs, independent media and bloggers is to be impersonated by adversaries that will create false profiles, websites or emails in their names. This is meant sometimes to create smearing campaigns, misleading information, social engineering or stealing one's identity in order to create noise, trust issues and data breaches that impact on the reputation of the individuals and collectives being impersonated. This is a frustrating problem that can affect on different levels your capacity to communicate and inform, and it can have different causes depending on where and how you are being impersonated.

It is important to know that there are many ways to impersonate someone (fake profiles in social media, cloned websites, spoofed emails, non-consensual publication of personal images and videos). Strategies may range from submitting take-down notices, proving original ownership, claiming copyright of the original website or information, or warning your networks and personal contacts through public or confidential communications. Diagnosing the problem and finding possible solutions to impersonation can be complicated. Sometimes it will be close to impossible to push a small hosting company to take down a website, and legal action may become necessary. It is good practice to set up alerts and monitor the internet for finding out if you or your organization are being impersonated.

This section of the Digital First Aid Kit will walk you through some basic steps to diagnose potential ways of impersonating and potential mitigation strategies to remove accounts, websites and emails impersonating you or your organization. 

If you are being impersonated, follow this questionnaire to identify the nature of your problem and find possible solutions.


## Workflow

### urgent_question

Do you fear for your physical integrity or wellbeing?

 - [Yes](#physical_sec_end)
 - [No](#diagnostic_start1)

### diagnostic_start1

Is the impersonation affecting you as an individual (someone is using your legal name and surname, or the nickname you base your reputation on) or as an organization/collective?

- [As an individual](#individual)
- [As an organization](#organization)
 
### individual

If you are being affected as an individual, you might want to alert your contacts. Take this step using a mail account, profile or website that is fully under your control.

- Once you have informed your contacts that you're being impersonated, proceed to the [next step](#diagnostic_start2).

### organization

> If you are being affected as a group, you might want to do a public communication. Take this step by using a mail account, profile or website that is fully under your control.

- Once you have informed your community that you're being impersonated, proceed to the [next step](#diagnostic_start2).

### diagnostic_start2

How are you being impersonated?

 - [A fake website is impersonating me or my group](#fake_website)
 - [Through a social network account](#social_network)
 - [Through non-consensual sharing of videos or images](#other_website)
 - [Through my email address or a similar address](#spoofed_email1)
 - [Through a PGP key connected to my email address](#PGP)
 - [Through a fake app that is imitating my app](#app1)

### social_network

On which social networking platform are you being impersonated?

- [Facebook](#facebook)
- [Twitter](#twitter)
- [Google](#google)
- [Instagram](#instagram)

###  facebook

> Follow [these instructions](https://www.facebook.com/help/174210519303259) for requesting the impersonating account to be deleted.
>
> Please note that it might take some time to receive an answer to your request. Save this page in your bookmarks and come back to this workflow in a few days.

Did it work?

- [Yes](#resolved_end)
- [No](#account_end)

### twitter

> Fill out [this form](https://help.twitter.com/forms/impersonation) for requesting the impersonating account to be deleted.
>
> Please note that it might take some time to receive an answer to your request. Save this page in your bookmarks and come back to this workflow in a few days.

Has this worked?

- [Yes](#resolved_end)
- [No](#account_end)

### google

> Fill out [this form](https://support.google.com/plus/troubleshooter/1715140) for requesting the impersonating account to be deleted.
>
> Please note that it might take some time to receive an answer to your request. Save this page in your bookmarks and come back to this workflow in a few days.

Has this worked?

- [Yes](#resolved_end)
- [No](#account_end)

### instagram

> Follow [these instructions](https://help.instagram.com/446663175382270) for requesting the impersonating account to be deleted.
>
> Please note that it might take some time to receive an answer to your request. Save this page in your bookmarks and come back to this workflow in a few days.

Has this worked?

- [Yes](#resolved_end)
- [No](#account_end)


### fake_website

> Check if this website is known as malicious by looking for its URL in the following online services:
>    
> - [circl.lu/urlabuse](https://circl.lu/urlabuse/)
> - [Virus Total.com](https://www.virustotal.com/)
> - [sitecheck.sucuri.net](https://sitecheck.sucuri.net/)
> - [urlscan.io](https://urlscan.io/)
> - [ThreatCrowd](https://www.threatcrowd.org/)

Is the domain known to be malicious?

 - [Yes](#malicious_website)
 - [No](#non-malicious_website)

### malicious_website

> Report the URL to Google Safe Browsing by filling out [this form](https://safebrowsing.google.com/safebrowsing/report_badware/).
>
> Please note that it might take some time to make sure that your report was successful. Meanwhile you can proceed to the next step for sending a takedown request to the hosting provider and domain registrar, or save this page in your bookmarks and come back to this workflow in a few days.

Has this worked?

- [Yes](#resolved_end)
- [No](#non-malicious_website)


### non-malicious_website

> You can try reporting the website to the hosting provider or domain registrar, asking for a takedown.
>
> If the website you want to report is using your content, one thing you may need to prove is that you are the legitimate owner of the original content. You can show this by presenting your original contract with the domain registrar and/or hosting provider, but you can also do a search on the [Wayback Machine](https://archive.org/web/), looking for both the URL of your website and the fake website. If the websites have been indexed there, you will find a history that may make it possible to show that your website existed before the fake website was published.
>
> To send a takedown request, you will also need to gather information on the fake website:
>
> - Go to [this website](https://network-tools.com/nslookup/) and find out the IP address (or addresses) of the fake website by entering its URL in the search form.
> - Write down the IP address or addresses.
> - Go to [this website](https://whois.domaintools.com/) and search both for the domain and the IP address/es of the fake website.
> - Record the name and abuse email address of the hosting provider and domain service. If included in the results of your search, also record the name of the website owner.
> - Write to the hosting provider and domain registrar of the fake website to request its takedown. In your message, include information on the IP address, URL, and owner of the impersonating website, as well as the reasons why it is abusive.
> - You can use [this template](https://accessnowhelpline.gitlab.io/community-documentation/352-Report_Fake_Domain_Hosting_Provider.html) to write to the hosting provider.
> - You can use [this template](https://accessnowhelpline.gitlab.io/community-documentation/343-Report_Domain_Impersonation_Cloning.html) to write to the domain registrar.
> 
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has this worked?

- [Yes](#resolved_end)
- [No](#web_protection_end)


### spoofed_email1

> For underlying technical reasons, it is quite difficult to authenticate emails. This is also why it is very easy to create forged sender addresses and spoofed emails.

Are you being impersonated through your email address, or a similar one, for example with the same user name, but a different domain?

- [I'm being impersonated through my email address](#spoofed_email2)
- [I'm being impersonated through a similar email address](#similar_email)


### spoofed_email2

> The person who is impersonating you might have hacked into your email account. To rule out this possibility, try changing your password.

Are you able to change your password?

- [Yes](#spoofed_email3)
- [No](#hacked_account)

### hacked_account

If you can't change your password, your email account is probably compromised.

- You can follow [this workflow](../../../account-access-issues) to solve this issue.

### spoofed_email3

> Email spoofing consists in email messages with a forged sender address. The message appears to have originated from someone or somewhere other than the actual source.
>
> Email spoofing is common in phishing and spam campaigns because people are more likely to open an email when they think it is coming from a legitimate source.
>
> If someone is spoofing your email, you should inform your contacts to warn them about the danger of phishing (do it from a mail account, profile or website that is fully under your control).
>
> If you think the impersonation was aimed at phishing or other malicious intents, you might also want to read the [I have received suspicious messages](../../../suspicious_messages) section.

Did the emails stop after you changed the password to your email account?

- [Yes](#compromised_account)
- [No](#secure_comms_end)


### compromised_account

> Probably your account was hacked into by someone who used it to send out emails to impersonate you. As your account was compromised, you might also want to read the [I lost access to my accounts](../../../account-access-issues/) section.

Has this helped solve your issue?

- [Yes](#resolved_end)
- [No](#account_end)


### similar_email

> If the impersonator is using an email address that is similar to yours but with a different domain or user name, it's a good idea to warn your contacts about this attempt at impersonating you (do it from a mail account, profile or website that is fully under your control).
>
> You might also want to read the [I have received suspicious messages](../../../suspicious-messages) section, as this impersonation might be aimed at phishing.

Has this helped solve your issue?

- [Yes](#resolved_end)
- [No](#secure_comms_end)


### PGP

Do you think your private PGP key might have been compromised, for example because you lost control of the device where it was stored?

- [Yes](#PGP_compromised)
- [No](#PGP_spoofed)

### PGP_compromised

Do you still have access to your private key?

- [Yes](#access_to_PGP)
- [No](#lost_PGP)

### access_to_PGP

> - Revoke your key.
>     - [Instructions for Enigmail](https://www.enigmail.net/documentation/Key_Management#Revoking_your_key_pair)
> - Create a new key pair and have it signed by people you trust.
> - Communicating through a channel you control, inform your contacts that you revoked your key and generated a new one.

Do you need more help solving your problem?

- [Yes](#secure_comms_end)
- [No](#resolved_end)


### lost_PGP

Do you have a revocation certificate?

- [Yes](#access_to_PGP)
- [No](#no_revocation_cert)


### no_revocation_cert

> - Create a new key pair and have it signed by people you trust.
> - Inform your contacts through a channel you control that they should use your new key and stop using the old one.

Do you need more help solving your problem?

- [Yes](#secure_comms_end)
- [No](#resolved_end)

### PGP_spoofed

Is your key signed by trusted people?

- [Yes](#signed_key)
- [No](#non-signed_key)

### signed_key

> Inform your contacts through a channel you control that someone is trying to impersonate you and tell them that they can recognize your actual key based on the signatures by trusted contacts.

Do you need more help solving your problem?

- [Yes](#secure_comms_end)
- [No](#resolved_end)

### non-signed_key

> - Have your key signed by people you trust.
> - Inform your contacts through a channel you control that someone is trying to impersonate you and tell them that they can recognize your actual key based on the signatures by trusted contacts.

Do you need more help solving your problem?

- [Yes](#secure_comms_end)
- [No](#resolved_end)

### other_website

> If you are being impersonated on a website, the first thing you need to do is understand where that website is hosted, who is managing it, and who has provided the domain name. This research is aimed at identifying the best way to request a takedown of the malicious content.
>
> Before you proceed with your investigation, if you are a EU citizen you can request Google to remove this website from their search results on your name.

Are you a citizen of the European Union?

- [Yes](#EU_privacy_removal)
- [No](#doxing_question)


### EU_privacy_removal

> Fill out [this form](https://www.google.com/webmasters/tools/legal-removal-request?complaint_type=rtbf&hl=en&rd=1) to remove this website from Google's search results on your name.
>
> What you will need:
>
> - A digital copy of an identification document (if you are submitting this request on behalf of someone else, you will need to supply identification documentation for them)
> - The URL(s) for the content containing the personal information you want removed
> - For each URL you provide, you will need to explain:
>     1. how the personal information identified above relates to the person on whose behalf this request is made
>     2. why you believe the personal information should be removed
>
> Please note that if you are signed into your Google Account, Google may associate your submission with that account.
>
> After submitting this form, you will have to wait for a reply from Google to verify the results have been removed.

Would you like to file a takedown request to remove the impersonating content from the website?

- [Yes](#doxing_question)
- [No, I would like to receive support](#account_end)

### doxing_question

Has the impersonator published personal information, or intimate videos, or images of you?

- [Yes](../../../harassed-online/questions/doxing_web)
- [No](#fake_website)

### app1

> If someone is spreading a malicious copy of your app or other software, it's a good idea to do a public communication to warn users to only download the legitimate version.
>
> You should also report the malicious app and request its takedown.

Where is the malicious copy of your app being distributed?

- [On Github](#github)
- [On Gitlab.com](#gitlab)
- [On Google Play Store](#playstore)
- [On Apple App Store](#apple_store)
- [On another website](#fake_website)

### github

> If the malicious software is hosted on Github, read [this guide](https://help.github.com/en/articles/guide-to-submitting-a-dmca-takedown-notice) for taking down content that violates copyright.
>
> It might take some time to wait for a response to your request. Save this page in your bookmarks and come back to this workflow in a few days.

Has this helped solve your issue?

- [Yes](#resolved_end)
- [No](#app_end)

### gitlab

> If the malicious software is hosted on Gitlab.com, read [this guide](https://about.gitlab.com/handbook/dmca/) for taking down content that violates copyright.
>
> It might take [some time](https://about.gitlab.com/handbook/support/workflows/services/gitlab_com/dmca.html) to wait for a response to your request. Save this page in your bookmarks and come back to this workflow in a few days.

Has this helped solve your issue?

- [Yes](#resolved_end)
- [No](#app_end)


### playstore

> If the malicious app is hosted on Google Play Store, follow [these instructions](https://support.google.com/legal/troubleshooter/1114905) for taking down content that violates copyright.
>
> It might take some time to wait for a response to your request. Save this page in your bookmarks and come back to this workflow in a few days.

Has this helped solve your issue?

- [Yes](#resolved_end)
- [No](#app_end)



### apple_store

> If the malicious app is hosted on the App Store, follow [these instructions](https://www.apple.com/legal/internet-services/itunes/appstorenotices/#/contacts?lang=en) for taking down content that violates copyright.
>
> It might take some time to wait for a response to your request. Save this page in your bookmarks and come back to this workflow in a few days.

Has this helped solve your issue?

- [Yes](#resolved_end)
- [No](#app_end)



### physical_sec_end

> If you are fearing for your physical wellbeing, please contact the organizations below who can support you.

:[](organisations?services=physical_sec) 


### account_end

> If you are still experiencing impersonation or your account is still compromised, please contact the organizations below who can support you.

:[](organisations?services=account&services=legal)


### app_end

> If the fake app hasn't been taken down, please contact the organizations below who can support you.

:[](organisations?services=account&services=legal)

### web_protection_end

> If your takedown requests have not been successful, you can try reaching out to the organizations below for further support.

:[](organisations?services=web_protection)

### secure_comms_end

> If you need help or recommendations on phishing, email security and encryption, and secure communications in general, you can reach out to these organizations:

:[](organisations?services=secure_comms)


### resolved_end

We hope this DFAK guide was useful. Please give us feedback [via email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

To prevent any further attempts at impersonating you, read the tips below.

### final_tips

- Create strong, complex and unique passwords for all your accounts.
- Consider using a password manager for creating and storing passwords so you can use many different passwords on different sites and services without having to memorize them. 
- Activate two-factor authentication (2FA) for your most important accounts. 2FA offers greater account security by requiring to use more than one method to log into your accounts. This means that even if someone were to get hold of your primary password, they could not access your account unless they also had your mobile phone or another secondary means of authentication.
- Verify your profiles in social networking platforms. Some platforms offer a feature for verifying your identity and linking it to your account.
- Map your online presence. Self-doxing consists in exploring open source intelligence on oneself to prevent malicious actors from finding and using this information for impersonating you.
- Set up Google alerts. You can get emails when new results for a topic show up in Google Search. For example, you can get information about mentions of your name or your organization/collective name.
- Capture your web page as it appears now for use as evidence in the future. If your website allows crawlers, you can use the Wayback Machine, offered by archive.org. Visit [this page](https://archive.org/web/) and click on the "Save Page Now" button.

#### resources

- [Create strong and unique passwords](https://ssd.eff.org/en/module/creating-strong-passwords)
- [Animated Overview: Using Password Managers to Stay Safe Online](https://ssd.eff.org/en/module/animated-overview-using-password-managers-stay-safe-online)
- [Choosing a password manager](https://accessnowhelpline.gitlab.io/community-documentation/295-Password_managers.html)
- [How to use KeePassXC - a secure open source password manager](https://ssd.eff.org/en/module/how-use-keepassxc)
- [Two-factor authentication (2FA)](https://www.accessnow.org/need-talk-sms-based-two-step-authentication/)
- [A guide to prevent doxing](https://guides.accessnow.org/self-doxing/self-doxing.html)
- [Archive your website](https://archive.org/web/)
