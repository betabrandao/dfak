---
layout: page
title: "I Lost My Device"
author: Hassen Selmi, Bahaa Nasr, Michael Carbone, past DFAK contributors
language: es
summary: "I lost my device, what should I do?"
date: 2019-03
permalink: /es/topics/lost-device
parent: /es/
---

# I lost my device

Is your device lost? Has it been stolen or seized by a third party? 

In these situations, it is important to take immediate steps to reduce the risk of someone else accessing your accounts, contacts, and personal information. 

This section of the Digital First Aid Kit will walk you through some basic questions so you can assess how to reduce possible harm related to losing a device.

## Workflow

### question_1

Is the device still missing?

 - [Yes it is missing](#device-missing)
 - [No it has been returned to me](#device-returned)

### device-missing

> It is good to reflect on what security protections the device had:
> 
> * Was access to the device protected by a password or other security measure?
> * Does the device have device encryption turned on?
> * What state was your device in when it was lost - Were you logged in? Was the device on but password-locked? Was it sleeping or hibernating? Completely turned off?

With these in mind, you can better understand how likely it is for someone else to get access to the content on your device. 

- [Let's remove the device's access to my accounts](#accounts)

### accounts

> List all the accounts this device had access to. This could be accounts for email, social media, messaging, dating, and banking, as well as accounts that may use this device for secondary authentication.
>
> For any accounts your device had access to (such as email, social media or web account), you should remove the authorization for this device for them. This can be done by logging into your accounts and removing the device from permitted devices. 
>
> * [Google Account](https://myaccount.google.com/device-activity)
> * [Facebook Account](https://www.facebook.com/settings?tab=security&section=sessions&view)
> * [iCloud Account](https://support.apple.com/en-us/HT205064)
> * [Twitter Account](https://twitter.com/settings/sessions)
> * [Yahoo Account](https://login.yahoo.com/account/activity)

Once you have completed delinking your accounts, let's secure your passwords that may have been on the device.

- [Okay let's deal with passwords](#passwords)

### passwords

> Reflect on any passwords directly saved to the device or any browsers that have stored saved passwords.
>
> Change the passwords for all accounts that are accessible by this device. If you don't use a password manager, strongly consider using one in order to better create and manage strong passwords.

After changing the passwords for your accounts on your phone, reflect on if you used any of these passwords for other accounts - if so, please change those passwords as well.

- [I used some of my passwords on the lost device for other accounts](#same-password)
- [All of my passwords that may have been compromised were unique](#2fa)

### same-password

Do you use the same password on other accounts or devices besides the lost device? If so, change the password on those accounts as they may also become compromised.

- [okay I have changed all relevant passwords now](#2fa)

### 2fa

> Enabling two-factor authentication on accounts that you think may be at risk of being accessed will reduce the likelihood that they can be accessed by someone else.
>
> Turn on two-factor authentication for all accounts that were accessible by this device. Please note that not all accounts support 2-factor authentication. For a list of services and platforms that support two-factor authentication please visit [Two Factor Auth](https://twofactorauth.org).

- [I have enabled two-factor authentication on my accounts to further secure them. I would like to try to find or erase my device](#find-erase-device)

### find-erase-device

> Think about what you used this device for - is there sensitive information on this device, such as contacts, location or messages? Can this data being leaked be problematic for you, your work, or others besides yourself?
>
> In some cases it might be helpful to remotely wipe the data on the device of a detained person, to prevent the data on it being abused and used against them or other activists. At the same time, this might be problematic for the detained person (especially in situations where torture and mistreatment are possible) - especially if the detained person was forced to give access to the device, data suddenly disappearing from the device might make the detaining authorities more suspicious. If you are facing a similar situation, read our section ["Someone I know has been arrested"](../../../../arrested).
>
> It is also worth noting that in some countries remote wiping might be legally challenging, as it could be interpreted as destruction of evidence.
>
> If you are clear about the direct and legal consequences for everyone involved, you can proceed with attempting to remotely wipe the device, by following instructions for your operating system:
>
> * [Android devices](https://support.google.com/accounts/answer/6160491?hl=en)
> * [iPhone or Mac](https://www.icloud.com/#find)
> * [iOS devices (iPhone and iPad) by using iCloud](https://support.apple.com/kb/ph2701)
> * [Windows phones](https://support.microsoft.com/en-us/help/11579/microsoft-account-find-and-lock-lost-windows-device) (section on "Find, ring, lock, or erase your Windows Phone")
> * [Blackberry phones](https://docs.blackberry.com/en/endpoint-management/blackberry-uem/12_9/blackberry-uem-self-service-user-guide/amo1375908155714)
> * For Windows or Linux devices, you may have installed software (such as anti-theft or anti-virus software) that allows you to remotely erase the data and the history of your device. If so, use it.

Whether you have managed or not to remotely wipe the information on the device, it is a good idea to inform your contacts.

- [Proceed to the next steps to find tips on how to inform your contacts about your device being lost.](#inform-network)

### inform-network 

> Besides your own accounts, your device will likely have information about others on it. This may include your contacts, communications with others, messaging groups.
>
> When considering informing your network and community about the lost device, use [harm reduction principles](../../../../arrested#harm-reduction) to ensure that by reaching out to others you are not putting them at greater risk.

Inform your network of the lost device. This could be done privately with key high-risk contacts, or posting a list of potentially compromised accounts publicly on your website or a social media account if you feel it is appropriate to do so.

- [I've informed my contacts](#review-history)


### review-history 

> If possible, review the connection history/account activity of all accounts connected to the device. Check to see if your account was used at a time when you were not online or if your account was accessed from an unfamiliar location or IP address.
>
> You can review your activity on some popular providers below:
>
> * [Google Account](https://myaccount.google.com/device-activity)
> * [Facebook Account](https://www.facebook.com/settings?tab=security&section=sessions&view)
> * [iCloud Account](https://support.apple.com/en-us/HT205064)
> * [Twitter Account](https://twitter.com/settings/sessions)
> * [Yahoo Account](https://login.yahoo.com/account/activity)

Have you reviewed your connection history?

- [I have, and found nothing suspicious](#check-settings)
- [I have, and found some suspicious activity](../../../account-access-issues/)

### check-settings

> Check the account settings of all accounts connected to the device. Have they been changed? For email accounts, check for auto-forwards, possible changes to the backup/reset email address of phone numbers, synchronization to different devices, including phones, computers or tablets, and permissions to applications or other account permissions.
>
> Repeat the review of the history of account activity at least once a week for a month, to ensure that your account continues to show no suspicious activity. 

- [My account activity history shows suspicious activity](../../../account-access-issues/)
- [My account activity history does not show any suspicious activity, however I will continue to review it over time.](#resolved_end)


### device-returned

> If your device was lost, taken by a third party or had to be handed over at a border crossing, but you have it back, be careful as you do not know who has had access to it. Depending on the level of risk you’re facing, you may want to treat the device as if it is now untrusted or compromised.
 
> Ask yourself the following questions and assess the risk that your device has been compromised:

> * How long was the device out of your sight?
> * Who potentially could have had access to it?
> * Why would they want access to it?
> * Are there signs that the device has been physically tampered with?

> If you do not trust your device anymore, consider wiping and reinstalling the device or getting a new device.

Would you like support in getting a replacement device?

- [Yes](#new-device_end)
- [No](#resolved_end)


### accounts_end

If you have lost access to your accounts or think someone might have accessed your accounts, please contact the organizations below who can support you.

:[](organisations?services=account)

### new-device_end

If you need external financial support to get a replacement device, please contact the organizations below who can support you.

:[](organisations?services=equipment_replacement)

### resolved_end

We hope this DFAK guide was useful. Please give us feedback [via email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Here is a series of tips to mitigate the risk of data leaks and unauthorized access to your accounts and information:

- Never leave your device unattended. If you need to do it, shut it down.
- Enable full-disk encryption. 
- Use a strong password to lock your device. 
- Enable the Find/Erase My Phone feature whenever possible, but please note that this could be used to track you or wipe your device out, if your associated account (Gmail/iCloud) is compromised. 

#### resources

* Consider using anti-theft software such as [Prey](https://preyproject.com)
* [Security in a Box tactics on securing sensitive files](https://securityinabox.org/en/guide/secure-file-storage/)
* [Tips on how to enable full-disk encryption](https://accessnowhelpline.gitlab.io/community-documentation/166-Full-Disk_Encryption.html)
