---
layout: page
title: "I Lost my Data"
author: Abir Ghattas, Alexandra Hache, Ramy Raoof
language: es
summary: "What to do in case you lost some data"
date: 2019-03-12
permalink: /es/topics/lost-data
parent: /es/
---

# I Lost my Data

Digital data can be very ephemeral and unstable and there are many ways you can lose it. Physical damage of your devices, termination of your accounts, erroneous deletion, software updates and software crashes can all propel a loss of data. Besides, sometimes you might not be aware of how your back-up system works, or simply have forgotten about your credentials or the route to find or recover your data.

This section of the Digital First Aid Kit will walk you through some basic steps to diagnose how you might have lost data and potential mitigation strategies to recover it.

Here is a questionnaire to identify the nature of your problem and find possible solutions.

## Workflow

### entry_point

> In this section we are mostly focusing on device-based data. For online content and credentials, we will be directing you to other sections of the Digital First Aid Kit.
>
> Devices include computers, mobile devices, external hard disks, USB sticks, and SD cards.

What type of data did you lose?

- [Online content](../../../account-access-issues)
- [Credentials](../../../account-access-issues)
- [Content on devices](#content_on_device)

### content_on_device

> Often your "lost" data is data that has simply been deleted - either accidentally or intentionally. In your computer, check your Recycle Bin or Trash. On Android devices, you might find the lost data in the LOST.DIR directory. If the missing or lost files cannot be located in the Recycle Bin or Trash of your operating system, it is possible that they haven't been deleted, but are in a different location than you expected. Try the search function in your operating system to locate them.
>
> Check also hidden files and folders, as the data you have lost might be there. Here is how you can do it on [Mac](https://www.macworld.co.uk/how-to/mac-software/show-hidden-files-mac-3520878/) and [Windows](https://support.microsoft.com/en-ie/help/4028316/windows-view-hidden-files-and-folders-in-windows-10). For Linux, go to your home directory in a file manager and type Ctrl + H. This option is available also on Android and iOS.
>
> Try searching for the exact name of your lost file. If that does not work, or if you are not sure of the exact name, you can try a wildcard search (i.e. if you lost a docx file, but aren't sure of the name, you can search `*.docx`) which will pull up only files with the `.docx` extension. Sort by the *Date Modified* in order to quickly locate the most recent files when using the wildcard search option.
>
> In addition to searching for the lost data on your device, ask yourself if you have emailed to or shared it with someone (including yourself) or added it to your cloud storage at any point. If that is the case, you can perform a search there and may be able to retrieve it or some version of it.
>
> To increase your chances of recovering data, stop using your devices immediately. Continuous writing on the hard drive might decrease the chances of finding/restoring data. Think of it as trying to recover a written message on a notepad - the more you write on the notepad pages, the less chances you have to find what you are looking for.

How did you lose your data?

- [The device where the data was stored suffered a physical damage](#tech_assistance_end)
- [The device where the data was stored was stolen/lost](#device_lost_theft_end)
- [The data was deleted](#where_is_data)
- [The data disappeared after a software update](#software_update)
- [The system or a software tool crashed and the data was gone](#where_is_data)


### software_update

> Sometimes, when you update or upgrade a software tool or the entire operating system, unexpected problems could happen, causing the system or software to stop working as it should, or some faulty behavior to happen, including data corruption. If you noticed a data loss just after a software or system update, it is worth considering to roll back to some previous state. Rollbacks are useful because they mean that your software or database can be restored to a clean and consistent state even after erroneous operations or software crashes have happened.
>
> The method to roll back a piece of software to a previous version depends on how that software is built - in some cases it is possible easily, in others it is not, and with some it would need some work around it. So you will need to do an online search on rolling back to older versions for that given software. Please note that rolling back is also known as "downgrading", so do a search with this term too. For operating systems such as Windows or Mac, each has their own methods for downgrading to previous version.
>
> - For Mac systems, visit the [Mac Support Center knowledge base](https://support.apple.com) and use the "downgrade" keyword with your macOS version to read more and find instructions.
> - For Windows systems, the procedure varies depending on if you want to undo a specific update or roll back the whole system. In both cases you can find instructions in the [Microsoft Support Center](https://support.microsoft.com), for example for Windows 10 you can look for the "How do I remove an installed update" question in this [FAQ page](https://support.microsoft.com/en-us/help/12373/windows-update-faq).

Was the software rollback useful?

- [Yes](#resolved_end)
- [No](#where_is_data)


### where_is_data

> Regular data backup is a good and recommended practice. Sometimes we forget that we have automatic backup enabled, so it is worth checking on your devices if you had that option enabled, and use that backup to restore your data. In case you don't, planning for future backups is recommended, and you can find more tips on how to set up backups in the [final tips of this workflow](#resolved_end).

To check if you have a backup of the data you lost, start asking yourself where the lost data was stored.

- [In storage devices (external hard disk, USB sticks, SD cards)](#storage_devices_end)
- [In a computer](#computer)
- [In a mobile device](#mobile)

### computer

What kind of operating system runs in your computer?

- [MacOS](#macos_computer)
- [Windows](#windows_computer)
- [Gnu/Linux](#linux_computer)

### macos_computer

> To check if your MacOS device had a backup option enabled, and use that backup to restore your data, check your [iCloud](https://support.apple.com/en-us/HT208682) or [Time Machine](https://support.apple.com/en-za/HT201250) options to see if there is any available backup.
>
> One place to look at is the running list of Recent Items, which keeps track of the apps, files and servers you have used during your past few sessions on the computer. To look for the file and reopen it, go to the Apple Menu in the upper-left corner, select Recent Items and browse the list of files. More details can be found [here](https://www.nytimes.com/2018/08/01/technology/personaltech/mac-find-lost-files.html).

Where you able to locate your data or restore it?

- [Yes](#resolved_end)
- [No](#tech_assistance_end)

### windows_computer

> To check if you have a backup enabled in your Windows machine, you can read [these instructions](https://support.microsoft.com/en-us/help/4027408/windows-10-backup-and-restore).
>
> Windows 10 includes the **Timeline** feature, which is meant to enhance your productivity by storing a record of the files you used, sites you browsed and other actions you made on your computer. If you can’t remember where you stored a document, you can click the Timeline icon in the Windows 10 task bar to see a visual log organized by date, and jump back to what you need by clicking the appropriate preview icon. This might help you locate a file that has been renamed. If Timeline is enabled, some of your PC activity — like files you edit in Microsoft Office — can also sync with your mobile device or another computer you use, so you might have a backup of your lost data in another device. You can read more on Timeline and how to use it, [here](https://www.nytimes.com/2018/07/05/technology/personaltech/windows-10-timeline.html ).

Where you able to locate your data or restore it?

- [Yes](#resolved_end)
- [No](#windows_restore)

### windows_restore

> In some cases there are free and open-source tools that can be helpful to find missing content and recover it. Sometimes they are limited in use, and most of the well-known tools cost some money.
>
> For example [Recuva](http://www.ccleaner.com/recuva ) is well-known free software for recovery. We recommend to give it a chance and see how it goes.

Was this information helpful in recovering your data (fully or partially)?

- [Yes](#resolved_end)
- [No](#tech_assistance_end)


### linux_computer

> Some of the most popular Linux distributions, like Ubuntu, have an in-built backup tool, for example [Déjà Dup](https://wiki.gnome.org/action/show/Apps/DejaDup) on Ubuntu. If a built-in backup tool is included in your operating system, you might have been prompted to enable automatic backups when you first started using your computer. Search your Linux distribution to see if it includes a built-in backup tool, and what the procedure is to check if it is enabled and restore data from backups.
>
> For Déja Dup on Ubuntu, you can have a look at [this article](https://www.dedoimedo.com/computers/ubuntu-deja-dup.html) for checking whether you have automatic backups enabled in your machine, and at [this page](https://wiki.gnome.org/DejaDup/Help/Restore/Full) for instructions on how to restore your lost data from an existing backup.

Was this information helpful in recovering your data (fully or partially)?

- [Yes](#resolved_end)
- [No](#tech_assistance_end)

### mobile

What operating system runs on your mobile?

- [iOS](#ios_phone)
- [Android](#android_phone)


### ios_phone

> You might have enabled an automatic backup with iCloud or iTunes. Read [this guide](https://support.apple.com/kb/ph12521?locale=en_US) to check if you have any existing backups and to learn how to restore your data.

Have you found your backup and recovered your data?

- [Yes](#resolved_end)
- [No](#phone_which_data)

###  phone_which_data

Which of the following applies to the data you lost?

- [It is app-generated data, e.g. contacts, feeds, etc.](#app_data_phone)
- [It is user-generated data, e.g. photos, videos, audio, notes](#ugd_phone)

### app_data_phone

> A mobile app is a software application designed to run on your mobile device. However most of these apps can also be accessed through a desktop browser. If you have experienced a loss of app-generated data in your mobile phones, try to access that app in your desktop browser, by logging in the app's web interface with your credentials for that app. You might find the data you lost in the browser interface.

Was this information helpful in recovering your data (fully or partially)?

- [Yes](#resolved_end)
- [No](#tech_assistance_end)

### ugd_phone

> User-generated data is the kind of data you create or generate through a specific app, and in case of data loss you will want to check if that app has backup settings enabled by default or enables a way to recover it. For example, if you use WhatsApp on your mobile and the conversations went missing or something faulty happened, you can recover your conversations if you enabled WhatsApp's recovery setting, or if you are using an app to create and keep notes with sensitive or personal information, it might also have a backup option enabled without you noticing sometimes.

Was this information helpful in recovering your data (fully or partially)?

- [Yes](#resolved_end)
- [No](#tech_assistance_end)

### android_phone

> Google has a service built into Android, called Android Backup Service. By default, this service backs up several types of data and associates it with the appropriate Google service, where you can also access it on the web. You can see your Sync settings by heading into Settings > Accounts > Google, then selecting your Gmail address. If you lost data of a kind that you were synchronizing with your Google account, you will probably be able to recover it by logging into your Google account through a web browser.

Was this information helpful in recovering your data (fully or partially)?

- [Yes](#resolved_end)
- [No](#phone_which_data)


### tech_assistance_end

> If your data loss has been caused by physical damage such has your devices falling on the floor or in the water, being exposed to an electricity power outage or other issues, the most probable situation is that you will need to perform a recovery of the data stored in the hard drives. If you do not know how to perform these operations, you should contact an IT person with hardware and electronic equipment maintenance that could help you. However, depending of your context and the sensitivity of the data you need to recover, we do not advice to contact any IT shop, and to try to give preference to IT persons you know and trust.


### device_lost_theft_end

> In case of lost or stolen devices, make sure to change all your passwords as soon as possible and to visit our specific [resource on what to do if a device is lost](../../../I lost my devices).
>
> If you're a member of civil society, and you need support to acquire a new device to replace the lost one, you can reach out to the organizations listed below.

:[](organisations?services=equipment_replacement)


### storage_devices_end

> In order to recover the data you have lost, remember that timing is important. For example recovering a file you accidentally deleted few hours or a day before might have higher success rates than a file you lost months before.
>
> Consider using a software tool for trying to recover the data you have lost (because of deletion or corruption) such as [Recuva for Windows](https://www.ccleaner.com/recuva/download), and for Gnu/Linux you can investigate the different software available for your distribution. Take into account that these tools do not always work, because your operative system may have written new data over your deleted information. Because of this, you should do as little as possible with your computer between deleting a file and attempting to restore it with a tool like Recuva.
>
> To increase your chances of recovering data, stop using your devices immediately. Continuous writing on the hard drive might decrease the chances of finding and restoring data.
>
> If none of the above options has worked for you, the most probable situation is that you will need to perform a recuperation of the data stored in the hard-disks. If you do not know how to perform these operations, you should contact an IT person with hardware and electronic equipment maintenance that could help you. However depending of your context and the sensitivity of the data you need to recover, we do not advice to contact any IT shop, and to try to give preference to IT persons you know and trust.


### resolved_end

We hope this DFAK guide was useful. Please give us feedback [via email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- Backups - In addition to the different tips above, it’s always a good idea to make sure you have backups - a lot of different backups, that you store somewhere other than the same place your data is! Depending on your context, opt for storing your backups in cloud services and in physical external devices you keep disconnected from your computer while connecting to the internet.
- For both types of backups, you should protect your data with encryption. Perform regular and incremental backups for your most important data and check that you have them ready before performing software or OS updates.
- Set up a structured folder system - Every person has its own way of organizing their important data and information, there is no one size fits all. Nonetheless, it is important that you consider setting up a folder system that suits your needs. By creating a consistent folder system, you can make your life easier by better knowing which folder and files should be frequently backed up for instance, where important information you are working on is located, where you should keep data containing personal or sensitive information about you and your collaborators, and so on. As usual, take a deep breath and some time for planning the type of data you produce or manage, and think of a folder system that can make it more consistent and tidy up.

#### resources

- [Security in a Box - Back-up Tactics](https://securityinabox.org/en/guide/backup/)
- [Official page on Mac backups](https://support.apple.com/mac-backup)
- [How to back up data regularly on Windows 10](https://support.microsoft.com/en-hk/help/4027408/windows-10-backup-and-restore)
- [How to enable regular backups on iPhone](https://support.apple.com/en-us/HT203977)
- [How to enable regular backups on Android](https://support.google.com/android/answer/2819582?hl=en).

