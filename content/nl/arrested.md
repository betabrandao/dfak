---
layout: sidebar.pug
title: "Someone I know has been arrested"
author: Peter Steudtner, Shakeeb Al-Jabri
language: nl
summary: "A friend, colleague, or family member has been detained by security forces. You wish to limit the impact of their arrest on them and anyone else who may be implicated with them."
date: 2019-03-13
permalink: /nl/arrested/
parent: Home
sidebar: >
  <h3>Read more about what to do if someone you know has been arrested:</h3>

  <ul>
    <li><a href="https://www.newtactics.org/search/solr/arrest">Getting support for campaigning on behalf of the detained person</a></li>
  </ul>
---

# Someone I know has been arrested

Arrests of human rights defenders, journalists, and activists put them and those with whom they work and live at great risk.

This guide is specially oriented at countries which are lacking in human rights and due process, or where authorities can circumvent legal procedures, or where non-state actors operate freely, detentions or arrests pose a greater risk to the victims, their colleagues and relatives.

In this guide we aim to alleviate the danger they face and limit the detainers’ access to sensitive data that may incriminate the victims and their colleagues, or that may be used to compromise other operations.

Taking care of the detainee as well as of the digital impacts of this detention might be exhausting and challenging. Try to find others to support you and coordinate your actions with the community involved.

Also it is important that you take care of yourself and others impacted by this arrestation, by:

- [taking care of your psycho-social well-being and needs](../self-care),
- [taking care of the legal side of your support work](***link to RaReNet section on organizations helping on legal matters***)
- [getting support for campaigning on behalf of the detained person](https://www.newtactics.org/search/solr/arrest)

If you feel technically or emotionally overwhelmed or (for whatever reason) not in a position to follow the steps outlined below, please reach out for support and guidance to the organizations listed [here](../support) that offer initial triage among their services.


## Make a plan

Before you act on the different sections we outline below, please go through the following steps:

- Try to read the whole guide and get an overview of all important areas of impact before taking action on singular aspects. The reason is that the different sections highlight different threat scenarios which might overlap, so you will need to build your own sequence of actions.
- Take time with friends or a team to perform the different risk assessments that are needed in the different sections.

<a name="harm-reduction"></a>
## Get ready before you act

Do you have reason to believe that this arrest or detention can lead to repercussions for the detainee's family members, friends, or colleagues, including yourself?

This guide will walk you through a series of steps to help provide solutions that may aid in reducing the exposure of the detainee and anyone who is involved with them. 

**Tips for coordinated response**:

In all situations where a person has been detained, the first thing to note is that often when such incidents occur, several friends or colleagues react at once, resulting in either duplicated or contradictory efforts. So it is important to remember that coordination and concerted action, at the local and international level, are necessary both to support the detainee and take care of everybody else within their support networks, family and friends.

- Establish a crisis team that will coordinate all activities of support, care, campaigning, etc.
- Involve family members, partners, etc. as much as possible (respecting their limits if, for example, they are overwhelmed).
- Establish clear goals for your support campaign (and review them frequently): for example, you may want to achieve the release of the detainee, to ensure their well-being, or to protect family and supporters and ensure their well-being as your most immediate goals.
- Agree on secure communication channels and frequency, as well as limitations (e.g. no communications between 10pm and 8am except for new emergencies or breaking news).
- Distribute tasks among the team members and reach out to third parties for support (analysis, advocacy, media work, documentation, etc.).
- Ask for further support  outside of the "crisis team" for catering to basic needs (e.g. regular meals, etc.)
   
**Digital security precautions**

If you have reasons to fear repercussions for yourself or other supporters, before tackling any digital emergencies connected to the detention of your friend, it is also crucial to take some preventative steps on the digital security level to protect yourself and others from immediate danger.

- Agree on which (secure) communication channels your network of supporters will use to coordinate mitigation and communicate about the detainee and further repercussions.
- Reduce the data on your devices to the necessary minimum, and protect the data on your devices with [encryption](https://ssd.eff.org/en/module/keeping-your-data-safe#1).
- Create [secure, encrypted backups](https://helplinedocs.accessnow.org/182-Secure_Backup.html) of all your data and keep them in a place which would not be found during searches or further detentions.
- Share passwords for devices, online accounts, etc. with a trusted person who is not in immediate danger.
- Agree on the actions to be taken (like account suspensions, remote device wiping, etc.) as a first reaction to your possible detention.

This guide will walk you through a series of steps to help provide solutions that may aid in reducing the exposure of the detainee and anyone who is involved with them, for example because information on contacts could be found on the detainee's devices, social media accounts, or mailbox.

## Risk assessment
### Reduce possible harm caused by our own actions

In general, you should try to base your actions on this question:

- What are the impacts of the single and combined actions on the detainee, but also on their communities, fellow activists, friends, family, etc., including yourself?

Each of the following sections will outline special aspects of this risk assessments.

Basic considerations are:

- Before erasing accounts, data, social media threads, etc., be sure that you have documented the content and information you are deleting, especially if you might have to restore that content or information, or need it for later evidence.
- If you delete or erase accounts or files, be aware that:
    - Authorities could interpret this as destruction or removal of evidence
    - This could make the situation of the detainee more difficult, if they gave access to these accounts or files, and the authorities cannot find them, as the detainee would appear as non-credible and actions against them might be triggered by this deletion.
- If you inform people about their personal information being stored in a device or account seized by the authorities, and this communication is intercepted, this might be used as additional evidence about the link to the detainee.
- Changes in communication procedures (including deletion of accounts, etc.) might trigger the attention of the authorities

### Informing Contacts

In general, it is impossible to determine, whether the detaining authorities have the capacity to map the detainee's contact network, and whether they have done it or not. So we have to assume the worst case, that they did it or are going to do it.

Before starting to inform the detainee's contacts, please assess the risk of informing them:
    
- Do you have a copy of the detainee's contacts list? Can you check who is on their contacts list, both in their devices, email accounts, and social networking platforms? Compile a list of possible contacts to get an overview of who might be affected.
- Is there a risk that informing the contacts could link them closer to the detainee and this might be (ab)used by the detaining authorities against them?
- Should everybody be informed, or just a certain set of people in the contacts?
- Who will inform which contacts? Who is in contact with whom already? What is the impact of this decision?
- Establish the safest communication channel, including personal meetings in spaces where there is no CCTV surveillance, for informing the implicated contacts.

### Document to Keep Evidence

Before you delete any content from websites, social media sites, threads etc., you might want to make sure, that you have documented these before. A reason for documenting would be to capture any sign or proof of abused accounts - like additional or impersonating content - or content you need as legal evidence.

Depending on the website or social media platform where you want to document feeds or online data, different approaches may be used:
    
- You can take screenshots of the relevant parts (make sure that timestamps, URLs, etc. are included in the screenshots).
- You can check that relevant websites or blogs are indexed in the [Wayback Machine](https://archive.org/web), or download the websites or blogs to your local machine.

*Remember that it is important to keep the information you downloaded in a secure device stored in a safe space.*

### Device Seizure

If any devices of the detained person were confiscated during or after the arrest, please read [I lost my devices](../topics/lost-device) guide, in particular the section on [remote wiping](../topics/lost-device/questions/find-erase-device), including recommendations in case of detention.


### Incriminating Online Data and Accounts

If the detainee has information on their devices that may be damaging to them or other people, it may be a good idea to try to limit the detainers' access to this information.

Before doing so, compare the risk posed by this information with the risk posed by security forces being upset due to the lack of access to this information (or taking legal action because of destruction of evidence). If the risk posed by this information is higher, you can proceed to removing relevant data and/or closing/suspending and delinking accounts by following the instructions below.

#### Suspend or Close Online Accounts

If you have access to the accounts you want to close, you can follow the processes of the different accounts. Please be sure to have a backup or copy of the deleted content and data! Be aware that after closing an account the content won't become immediately inaccessible: in the case of Facebook, for example, it can take up to two weeks for the content to be deleted from all servers.

If you don't have access to the detainee's accounts or you need more urgent action on social media accounts, please get support by the organizations listed [here](../support) that offer account security among their services.

#### Delink Accounts from Devices

Sometimes you may also want to delink accounts from devices, as this link may give access to sensitive data to anyone who controls that device. To do this, you can follow [these instructions](../topics/lost-device/questions/accounts).

Don't forget to delink [online bank accounts](#online_bank_accounts) from the devices.


#### Change Passwords

If you decide not to close or suspend accounts, it can still be useful to change their passwords, by following [these instructions](../topics/lost-device/questions/passwords).

Also consider enabling 2-factor authentication to increase the security of the detainee's accounts, by following [these instructions](../topics/lost-device/questions/2fa).

In case the same passwords were used on different accounts, you should change all affected passwords on those accounts as they may also become compromised.

**Other tips on changing passwords**

- Use a password manager (like [KeepassXC](https://keepassxc.org)) for documenting the changed passwords for further use or to hand them over to the detainee after release.
- Make sure to tell the detainee, at the latest after their release, of the changed passwords and give them ownership back.


#### Remove Group Membership and Unshare Folders

If the detainee is in any groups (such as, but not limited to) Facebook groups, WhatsApp, Signal, or Wire group chats, or can access shared folders online, and their presence in these groups gives their detainers access to privileged and potentially dangerous information, you might want to remove them from groups or shared online spaces.

**Instructions on how to remove group membership on different instant messaging services:**

- [WhatsApp](https://faq.whatsapp.com/en/android/26000116/?category=5245251)
- Telegram - The person who created the group can remove participants by selecting "Group Info" and swiping the user they want to remove to the left.
- Wire
    - [instructions for mobile](https://support.wire.com/hc/en-us/articles/203526410)
    - [instructions for desktop app](https://support.wire.com/hc/en-us/articles/203526400-How-can-I-remove-someone-from-a-group-conversation-)
- Signal - You cannot remove participants from Signal groups - what you can do is create a new group excluding the account of the arrested person.

**Instructions on how to unshare folders on different online services:**

- [Facebook](https://www.facebook.com/help/211909018842184/)
- GDrive - First search for the files by using the "to:" search operator (`to:username@gmail.com`), then select all of the results and click the Share icon. Click Advanced, remove the address from the dialogue and save.
- [Dropbox](https://www.dropbox.com/help/files-folders/unshare-folder#remove-member)
- [iCloud](https://support.apple.com/en-us/HT201081) 


### Feed Deletion

In some cases you may want to remove content from the detainee's social media timelines or from other feeds linking to their account, because it might be abused as evidence against them or to create confusion and conflict inside the detainee's community or discredit them.

Some services make the deletion of feeds and posts from accounts and timelines easier. Guides for Twitter, Facebook and Instagram are linked below. Please make sure, that you have documented the content you want to delete, in case you might still need it as evidence in case of tampering etc.

- For Twitter, you can use [Tweet Deleters](https://tweetdeleter.com/).
- For Facebook, you can follow [this guide](https://medium.com/@louisbarclay/how-to-delete-your-facebook-news-feed-6c99e51f1ef6), based on an app for Chrome Browser called [Nudge](https://chrome.google.com/webstore/detail/nudge/dmhgdnbkjkejeddddlklojinngaideac).
- For Instagram, you can follow [these instructions](https://help.instagram.com/997924900322403) (but be aware, that people with access to your account settings will be able to see the history of most of your interactions even after deleting content).


### Delete Harmful Online Associations

If there is any online information where the detainee is named which might have negative implications for them or their contacts, it's a good idea to delete it if this won't harm the detainee further.

- Make a list of online spaces and information which needs to be removed or changed.
- If you have identified content to be removed or changed, you might want to back it up before proceeding with its deletion or with takedown requests.
- Assess if the removal of the name of the detainee might have a negative impact on their situation (e.g. removing their names from the list of staff of an organization might protect the organization, but it may also remove the justification for the detainee, for example that they were working for that organization).
- If you have access to the respective websites or accounts, change or remove the sensitive content and information.
 - If you do not have access, request people with access to remove the sensitive information.
- You can find instructions for removing content on Google services [here](https://support.google.com/webmasters/answer/6332384?hl=en#get_info_off_web)
- Check whether the websites containing information have been indexed by the Wayback Machine or Google Cache. If so, this content should be removed too.

<a name="online_bank_accounts"></a>
### Online Bank Accounts

Very often, bank accounts are managed and accessed online, and verification via mobile devices can be necessary for transactions or even only to access the online account. If a detainee is not accessing their bank accounts for a longer period of time, this might have implications for the detainee's financial situation and their ability to access the account. In these cases, make sure to:

- Delink seized devices from the detainee's bank account/s.
- Get authorization and power of attorney by the detainee to be able to operate their bank account on their behalf early in the process (in agreement with their relatives).


## Final tips

- Make sure, that you return all ownership of data back to the detainee after release
- Read [these tips](../topics/lost-device/questions/device-returned) on how to deal with seized devices after they are returned by the authorities.

