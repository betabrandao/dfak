---
layout: page
title: Contact form
author: mfc
language: nl
summary: Contact methods
date: 2018-09
permalink: /nl/contact-methods/contact-form.md
parent: /nl/
published: true
---

A contact form will most likely protect your message to the recipient organization so that only you and the recipient organization can read it. However the fact that you contacted the organization may be accessible by governments, law enforcement agencies, or other parties with the necessary technical equipment. If you would like to better protect the fact that you are reaching out to this organization, use the Tor Browser to access the website with contact form.
