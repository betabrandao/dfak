---
layout: page
title: Phone number
author: mfc
language: nl
summary: Contact methods
date: 2018-09
permalink: /nl/contact-methods/phone-number.md
parent: /nl/
published: true
---

Mobile and landline phone communications are not encrypted to your recipients, so the content of your conversation and information about who you are calling is accessible by governments, law enforcement agencies, or other parties with the necessary technical equipment. 
