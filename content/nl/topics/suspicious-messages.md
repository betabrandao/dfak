---
layout: page
title: "I Received a Suspicious Message"
author: Abir Ghattas, Donncha Ó Cearbhaill, Claudio Guarnieri, Michael Carbone
language: nl
summary: "I received a suspicious message, link or email, what should I do about it?"
date: 2019-03-12
permalink: /nl/topics/suspicious-messages
parent: /nl/
---

# I Received a Suspicious Message

You may receive _suspicious_ messages to your email inbox, social media accounts and/or messaging applications. The most common form of suspicious emails is phishing emails. Phishing emails aim to trick you into giving up your personal, financial, or account information. They may ask you to visit a fake website or call a fake customer service number. Phishing emails can also contain attachments that install malicious software on your computer when opened.

If you are not certain about the authenticity of the message you received, or what to do about it, you can use the following questionnaire as a guiding tool to further diagnose the situation or to share the message with external trusted organizations that will provide you with a more detailed analysis of your message.

Keep in mind that receiving a suspicious email does not necessarily mean that your account has been compromised. If you think an email or message is suspicious, don't open it. Don't reply to the email, don't click any links, and don't download any attachments.

## Workflow

### intro

Have you performed any action on the message or link?

- [I clicked a link](#link_clicked)
- [I entered credentials](#account_security_end)
- [I download a file](#device_security_end)
- [I replied with information](#reply_personal_info)
- [I have taken no action yet](#do_you_know_sender)

### do_you_know_sender

Do you recognise the sender of the message? Be aware that the sender of the message may be [spoofed](https://en.wikipedia.org/wiki/Email_spoofing) to appear to be someone you trust.

 - [It a person or organisation who I know](#known_sender)
 - [It is a service provider (such as an email provider, hosting, social media or bank)](#service_provider)
 - [The message was sent by an unknown person or organisation](#share)

### known_sender

> Can you reach the sender using another communication channel? For example if you received an email, can you verify it directly with the sender by phone or WhatsApp? Be sure to use an existing contact method. You cannot necessarily trust a phone number in the signature of a suspicious message.

Did you confirm that sender is the one that sent you this message?

 - [Yes](#resolved_end)
 - [No](#share)

### service_provider

> In this scenario, a service provider is any company or brand that provides services you use or are subscribed to. This list can contain your email provider (Google, Yahoo, Microsoft, Prontonmail...), your social media provider (Facebook, Twitter, Instagram...), or online platforms that have your financial information (Paypal, Amazon, banks, Netflix...).
>
> Is there a way that you can check if the message is authentic? Many service providers will also provide copies of notifications or other documents in your account page. For example, if the message is from Facebook, it should be included in your [list of notification emails](https://www.facebook.com/settings?tab=security&section=recent_emails), or if it is from your bank, you can call your customer service.

Choose one of the below options:

 - [I was able to verify it is a legitimate message from my service provider](#resolved_end)
 - [I wasn't able to verify the message](#share)
 - [I am not subscribed to this service and/or expecting a message from them](#share)

### link_clicked

> In some suspicious messages, the links can take you to fake login pages that will steal you credentials or other type of pages that might steal your personal or financial information. The link might in some cases asks you to download attachments that install malicious software on your computer when opened.

Can you tell what happened after your clicked on the link?

 - [It asked me to enter credentials](#account_security_end)
 - [It downloaded a file](#device_security_end)
 - [Nothing happened but I am not sure](#device_security_end)
 - [I didn't click on the link](#share)

### reply_personal_info

> Depending on the type of information you shared, you may need to take immediate action.

What kind of information did you share?

- [I shared confidential account information](#account_security_end)
- [I shared public information](#share)
- [I'm not sure how sensitive the information was and I need help](#help_end)


### share

> Sharing your suspicious message can help protect your colleagues and community who may also be affected.
>
> To share your suspicious message, make sure to include the message itself as well as information about the sender. If the message was an email, please make sure to include the full email including headers using the [following guide from CIRCL](https://www.circl.lu/pub/tr-07/).

Do you need further help?

- [Yes, I need more help](#help_end)
- [No, I've solved my problem](#resolved_end)


### device_security_end

> In case some files were downloaded to your device, the security of your device may be at risk!

Please contact the organizations below who can support you. Afterward, please [share your suspicious message](#share).

:[](organisations?services=device_security)


### account_security_end

> In case your entered your credentials, the security of your accounts may be at risk!
>
> If you think your account is compromised, we recommend you also follow the Digital First Aid Kit workflow on [compromised accounts](../../../account-access-issues).
>
> We suggest you inform your community about this phishing campaign, and share the suspicious message with organizations that can analyze them.

Would you like to share information on the message you received or do you need further assistance before?

- [I want to share the suspicious message](#share)
- [I need more help to secure my account](#account_end)
- [I need more help to analyze the message](#analysis_end)

### help_end

> You should seek help from your colleagues or others to better understand and risks from the information you shared. Other people in your organisation or network may also have received similar requests.

Please contact the organizations below who can support you. Afterward, please [share your suspicious message](#share).

:[](organisations?services=24_7_digital_support)


### account_end

If your account has been compromised and you need help to secure it, please contact the organizations below who can support you.

:[](organisations?services=account)


### analysis_end

The following organizations can receive your suspicious message and investigate it further for you:

:[](organisations?services=forensic&services=vulnerabilities_malware)

### resolved_end

We hope this DFAK guide was useful. Please give us feedback [via email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)


### final_tips

The first rule to remember: Never give out any personal information in emails. No institution, bank or otherwise will ever ask for this information via email. It may not always be easy to tell whether an email or website is legitimate, but there are some tips that can help you assess the email you have received.

* Sense of urgency: suspicious emails typically warn of a sudden change to an account and ask you to act immediately to verify your account.
* In the body of an email, you might see questions asking you to “verify” or “update your account” or “failure to update your records will result in account suspension”. It is usually safe to assume that no credible organization to which you have provided your information will ever ask you to re-enter it, so do not fall for this trap.
* Beware of unsolicited messages, attachments, links and login pages.
* Watch out for spelling and grammar errors.
* Click to view the full sender address, not only the displayed name.
* Be mindful of shortened links - they can hide a malicious link behind it.
* When you hover your mouse over a link, the actual URL you are being directed to is displayed in a popup or at the bottom of your browser window.

#### Resources

Here are a number of resources to spot suspicious messages and avoid being phished.

* [EFF: How to Avoid Phishing Attacks](https://ssd.eff.org/en/module/how-avoid-phishing-attacks)
* [PhishMe: How to Spot a Phish infographic](https://cofense.com/wp-content/uploads/2016/07/phishme-how-to-spot-a-phish.pdf)
* [Citizen Lab: Communities @ Risk report](https://targetedthreats.net) - [Appendix](https://targetedthreats.net/media/5-Appendix.pdf) includes examples of real phishing emails including a categorization of how personalized and targeted they are.