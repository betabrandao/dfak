---
name: Digital Defenders Partnership
website: https://www.digitaldefenders.org
logo: DDP_logo_zwart_RGB.png
languages: Español, English, Français, Português, Deutsch, Nederlands, Русский
services: grants_funding, in_person_training, org_sec, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware
beneficiaries: activists, journalists, hrd, lgbti, women, youth, cso
hours: Monday-Thursday 9am-5pm CET
response_time: 4 days
contact_methods: email, phone, mail
email: team@digitaldefenders.org
mail: Raamweg 16, 2596 HL Den Haag
phone: +31 070 376 5500
---

The Digital Defenders Partnership (DDP) aim is to protect and advance internet freedom, and to keep the internet free from emerging threats, specifically in repressive environments. We coordinate emergency support for individuals and organizations such as human rights defenders, journalists, civil society activists, and bloggers. We take a people-centered approach, focusing on our core values of transparency, human rights, inclusivity & diversity, equality, confidentiality, and freedom. DDP was formed in 2012 by the Freedom Online Coalition (FOC).

The DDP has three different types of funding that address urgent emergency situations, as well as longer-term grants focused on building capacity within an organization. Furthermore, we coordinate a Digital Integrity Fellowship where organizations receive personalized digital security and privacy trainings, and a Rapid Response Network program.
