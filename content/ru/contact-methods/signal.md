---
layout: page
title: Signal
author: mfc
language: ru
summary: Contact methods
date: 2018-09
permalink: /ru/contact-methods/signal.md
parent: /ru/
published: true
---

Using Signal will ensure the content of your message is encrypted only to the recipient organization, and only you and your recipient will know the communications took place. Be aware that Signal uses your phone number as your username so you will be sharing your phone number with the organization you are reaching out to.

Resources: [How to: Use Signal for Android](https://ssd.eff.org/en/module/how-use-signal-android), [How to: Use Signal for iOS](https://ssd.eff.org/en/module/how-use-signal-ios), [How to Use Signal Without Giving Out Your Phone Number](https://theintercept.com/2017/09/28/signal-tutorial-second-phone-number/)