---
layout: page
title: "I cannot access my account"
author: RaReNet
language: ru
summary: "Are you having a problem accessing an email, social media or web account? Does an account show activity that you do not recognize? There are many things you can do to mitigate this problem."
date: 2015-08
permalink: /ru/topics/account-access-issues/
parent: /ru/
---


# I lost access to my account

Social media and communications accounts are widely used by civil society members to communicate, share knowledge and advocate their causes. As a consequence, these accounts are highly targeted by malicious actors, who often try to compromise these accounts, causing harm to civil society members and their contacts.

This guide is here to help you in case you have lost access to one of your accounts because it was compromised.

Here is a questionnaire to identify the nature of your problem and find possible solutions.

## Workflow

### Password_Typo

> Sometimes we might not be able to log into our account because we are mistyping the password, or because our keyboard language setting is not the one we usually use or we have CapsLock on.
>
> Try writing your username and password in a text editor and copying them from the editor to paste them into the log-in form. Also make sure your keyboard language settings are right, or if you have CapsLock on?

Did the above suggestions help you log into your account?

- [Yes](#resolved_end)
- [No](#What_Type_of_Account_or_Service)

### What_Type_of_Account_or_Service

What type of account or service have you lost access to?

- [Facebook](#Facebook)
- [Facebook Page](#Facebook_Page)
- [Twitter](#Twitter)
- [Google/Gmail](#Google)
- [Yahoo](#Yahoo)
- [Hotmail/Outlook/Live](#Hotmail)
- [Protonmail](#Protonmail)
- [Instagram](#Instagram)
<!--- - [AddOtherServiceLink](#service_Name) -->


### Facebook_Page

Does the page have other admins?

- [Yes](#Other_admins_exist)
- [No](#Facebook_Page_recovery_form)

### Other_admins_exist

Do the other admin(s) have the same issue?

- [Yes](#Facebook_Page_recovery_form)
- [No](#Other_admin_can_help)

### Other_admin_can_help

> Please ask other admins to add you to the page admins again.

Did this fix the issue?

- [Yes](#Fb_Page_end)
- [No](#account_end)


### Facebook_Page_recovery_form

> Please log into Facebook and use [this form to recover the page](https://www.facebook.com/help/contact/164405897002583)).
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)


<!---=========================================================
//GoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogle
=========================================================-->

### Google

Do you have access to the connected recovery email/mobile?

- [Yes](#I_have_access_to_recovery_email_google)
- [No](#Recovery_Form_google)

### I_have_access_to_recovery_email_google

Check if you received a "Critical security alert for your linked Google Account" email from Google. Did you receive it?

- [Yes](#Email_received_google)
- [No](#Recovery_Form_google)

### Email_received_google

Please check if there is a "recover your account" link. Is it there?

- [Yes](#Recovery_Link_Found_google)
- [No](#Recovery_Form_google)

### Recovery_Link_Found_google

> Please use the "recover your account" link to recover your account.

Were you able to recover your account?

- [Yes](#resolved_end)
- [No](#Recovery_Form_google)

### Recovery_Form_google

> Please try [this recovery form to recover your account](https://support.google.com/accounts/answer/7682439?hl=en).
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)


<!---=========================================================
//YahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahoo
=========================================================-->

### Yahoo

Do you have access to the connected recovery email/mobile?

- [Yes](#I_have_access_to_recovery_email_yahoo)
- [No](#Recovery_Form_Yahoo)

### I_have_access_to_recovery_email_yahoo

Check if you received a "Password change for your Yahoo account" email from Yahoo. Did you receive it?

- [Yes](#Email_received_yahoo)
- [No](#Recovery_Form_Yahoo)

### Email_received_yahoo

Please check if there is a "Recover your account here" link. Is it there?

- [Yes](#Recovery_Link_Found_Yahoo)
- [No](#Recovery_Form_Yahoo)

### Recovery_Link_Found_Yahoo

> Please use the "Recover your account here" link to recover your account.

Were you able to recover your account?

- [Yes](#resolved_end)
- [No](#Recovery_Form_Yahoo)

### Recovery_Form_Yahoo

> Please follow [these instructions to recover your account](https://help.yahoo.com/kb/account/fix-problems-signing-yahoo-account-sln2051.html?impressions=true).
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)


<!---=========================================================
TwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitter
//========================================================= -->

### Twitter

Do you have access to the connected recovery email/mobile?

- [Yes](#I_have_access_to_recovery_email_Twitter)
- [No](#Recovery_Form_Twitter)

### I_have_access_to_recovery_email_Twitter

Check if you received a "Your Twitter password has been changed" email from Twitter. Did you receive it?

- [Yes](#Email_received_Twitter)
- [No](#Recovery_Form_Twitter)

### Email_received_Twitter

Please check if the message contains a "recover your account" link. Is it there?

- [Yes](#Recovery_Link_Found_Twitter)
- [No](#Recovery_Form_Twitter)

### Recovery_Link_Found_Twitter

> Please use the "recover your account" link to recover your account.

Were you able to recover your account?

- [Yes](#resolved_end)
- [No](#Recovery_Form_Twitter)

### Recovery_Form_Twitter

>Please try [this recovery form to recover this account](https://support.google.com/accounts/answer/7682439?hl=en).
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)


<!---=========================================================
//ProtonmailProtonmailProtonmailProtonmailProtonmailProtonmailProtonmail
//========================================================= -->

### Protonmail

> Please try [this recovery form to recover your account](https://protonmail.com/support/knowledge-base/reset-password/).
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)

<!---==================================================================
//MicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlook
//================================================================== -->

### Hotmail

Do you have access to the connected recovery email/mobile?

- [Yes](#I_have_access_to_recovery_email_Hotmail)
- [No](#Recovery_Form_Hotmail)

### I_have_access_to_recovery_email_Hotmail

Check if you received a "Microsoft account password change" email from Hotmail. Did you receive it?

- [Yes](#Email_received_Hotmail)
- [No](#Recovery_Form_Hotmail)

### Email_received_Hotmail

Please check if the message contains a "Reset your password" link. Is it there?

- [Yes](#Recovery_Link_Found_Hotmail)
- [No](#Recovery_Form_Hotmail)

### Recovery_Link_Found_Hotmail

> Please use the "Reset your password" link to recover your account.

Were you able to recover your account with "Reset your password" link?

- [Yes](#resolved_end)
- [No](#Recovery_Form_Hotmail)

### Recovery_Form_Hotmail

> Please try [this recovery form to recover your account](https://account.live.com/acsr).
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)


### Facebook

Do you have access to the connected recovery email/mobile?

- [Yes](#I_have_access_to_recovery_email_Facebook)
- [No](#Recovery_Form_Facebook)

### I_have_access_to_recovery_email_Facebook

Check if you received a "Facebook password change" email from Facebook. Did you receive it?

- [Yes](#Email_received_Facebook)
- [No](#Recovery_Form_Facebook)

### Email_received_Facebook

Does the email contain a message saying "If you didn't do this, please secure your account" with a link?

- [Yes](#Recovery_Link_Found_Facebook)
- [No](#Recovery_Form_Facebook)

### Recovery_Link_Found_Facebook

> Please use the "Recover your account here" link in the message to recover your account.

Were you able to recover your account by clicking on the link?

- [Yes](#resolved_end)
- [No](#Recovery_Form_Facebook)

### Recovery_Form_Facebook

> Please try [this recovery form to recover your account](https://www.facebook.com/login/identify).
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)

<!--- ==================================================================
InstagramInstagramInstagramInstagramInstagramInstagramInstagramInstagram
//================================================================== not yet tested-->

### Instagram

Do you have access to the connected recovery email/mobile?

- [Yes](#I_have_access_to_recovery_email_Instagram)
- [No](#Recovery_Form_Instagram)

### I_have_access_to_recovery_email_Instagram

Check if you received a "Your Instagram password has been changed" email from Instagram. Did you receive it?

- [Yes](#Email_received_Instagram)
- [No](#Recovery_Form_Instagram)

### Email_received_Instagram

Please check if there is a recovery link. Is it there?

- [Yes](#Recovery_Link_Found_Instagram)
- [No](#Recovery_Form_Instagram)

### Recovery_Link_Found_Instagram

> Please use the "Recover your account here" link to recover your account.

Were you able to recover your account?

- [Yes](#resolved_end)
- [No](#Recovery_Form_Instagram)

### Recovery_Form_Instagram

> Please try [this recovery form to recover your account](https://help.instagram.com/149494825257596?helpref=search&sr=1&query=hacked).
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)


### Fb_Page_end

We're really glad that your problem is solved. Please read these recommendations to help you minimize the possibility of you loosing access to your page in the future:

- Activate 2FA for all admins on the page.
- Assign admin roles only to people you trust and who are responsive.

### account_end

If the procedures suggested in this workflow haven't helped you recover access to your account, you can reach out to the following organizations to ask for further help:

:[](organisations?services=account)

### resolved_end

We hope this DFAK guide was useful. Please give us feedback [via email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Please read these recommendations to help you minimize the possibility of you loosing access to your accounts in the future 

- It is always a good advice to turn on two-factor authentication (2FA) for all your accounts that support it.
- Never use the same password for more than one account. If you do so, please change them as soon as possible.
- Using a password manager will help you create and remember unique, strong passwords for all your accounts.
- Be cautious when using open public untrusted wifi networks, and possibly connect through them through a VPN or Tor.

#### resources

* [Security in a Box - Create and Maintain Strong Passwords](https://securityinabox.org/en/guide/passwords/)
* [Security Self-Defense - Protecting Yourself on Social Networks](https://ssd.eff.org/en/module/protecting-yourself-social-networks)





<!--- Edit the following to add another service recovery workflow:
#### service_name

Do you have access to the connected recovery email/mobile?

- [Yes](#I_have_access_to_recovery_email_google)
- [No](#Recovery_Form_google)

### I_have_access_to_recovery_email_google

Check if you received a "[Password Change Email Subject]" email from service_name. Did you receive it?

- [Yes](#Email_received_service_name)
- [No](#Recovery_Form_service_name

### Email_received_service_name

> Please check if there is a "recover your account" link. Is it there?

- [Yes](#Recovery_Link_Found_service_name)
- [No](#Recovery_Form_service_name)

### Recovery_Link_Found_service_name

> Please use the [Recovery Link Description](URL) link to recover your account.

Were you able to recover your account with "[Recovery Link Description]" link?

- [Yes](#resolved_end)
- [No](#Recovery_Form_service_name)

### Recovery_Form_service_name

> Please try this recovery form to recover this account: [Link to the standard recovery form].
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)

-->
