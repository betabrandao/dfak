---
layout: page
title: "Are you being targeted by online harassment?"
author: Floriana Pagano
language: ru
summary: "Are you being targeted by online harassment?"
date: 2019-04-01
permalink: /ru/topics/harassed-online/
parent: /ru/
---

# Are you being targeted by online harassment?

The Internet, and social media platforms in particular, have become a critical space for civil society members and organizations, especially for women, LGBTIQ people, and other minorities, to express themselves and make their voices heard. But at the same time, they have also become spaces where these groups are easily targeted for expressing their views. Online violence and abuse denies women, LGBTIQ persons, and many other unprivileged people the right to express themselves equally, freely, and without fear.

Online violence and abuse has many different forms, and perpetrators can often rely on impunity, also due to a lack of laws that protect victims of harassment in many countries, but most of all because protection strategies need to be tweaked creatively depending on what kind of attack is being launched.

It is therefore important to identify the typology of the attack targeting us to decide what steps we can take.

This section of the Digital First Aid Kit will walk you through some basic steps to plan how to get protected against the attack you are suffering.

If you are targeted by online harassment, follow this questionnaire to identify the nature of your problem and find possible solutions.

## Workflow

### physical_wellbeing

Do you fear for your physical integrity or wellbeing?

- [Yes](#physical_risk_end)
- [No](#no_physical_risk)

### no_physical_risk

Do you think the attacker has accessed or is accessing your device?

 - [Yes](#device_compromised)
 - [No](#account_compromised)

### device_compromised

> Change the password to access your device for a unique, long and complex one:
>
> - [Mac OS](https://support.apple.com/en-us/HT202860)
> - [Windows](https://support.microsoft.com/en-us/help/14087/windows-7-change-your-windows-password)
> - [iOS - Apple ID](https://support.apple.com/en-us/HT201355)
> - [Android](https://support.google.com/accounts/answer/41078?co=GENIE.Platform%3DAndroid&hl=en)

Has the attacker been effectively locked out of your device?

 - [Yes](#account_compromised)
 - [No](../../../device-acting-suspiciously)

### account_compromised

> If someone got access to your device, they might have also accessed your online accounts, so they could be reading your private messages, identify your contacts, and publish posts, images, or videos impersonating you.

Have you noticed posts or messages disappearing, or other activities that give you good reason to think your account may have been compromised? Review also your send folder for possible suspicious activity.

 - [Yes](../../../account-access-issues)
 - [No](#impersonation)

### impersonation

Is someone impersonating you?

- [Yes](../../../impersonated)
- [No](#doxing)

### doxing

Has someone published private information or pictures without your consent?

- [Yes](#doxing_yes)
- [No](#hate_speech)

### doxing_yes

Where have your private information or pictures been published?

- [On a social networking platform](#doxing_sn)
- [On a website](#doxing_web)

### doxing_sn

> If your private information or pictures have been published in a social media platform, you can report a violation of the community standards following the reporting procedures provided to users by social networking websites. You will find instructions for the main platforms in the following list:
>
> - [Google](https://www.cybercivilrights.org/online-removal/#google)
> - [Facebook](https://www.cybercivilrights.org/online-removal/#facebook)
> - [Twitter](https://www.cybercivilrights.org/online-removal/#twitter)
> - [Tumblr](https://www.cybercivilrights.org/online-removal/#tumblr)
> - [Instagram](https://www.cybercivilrights.org/online-removal/#instagram)

Have the information or media been deleted?

 - [Yes](#one_more_persons)
 - [No](#harassment_end)

### doxing_web

> Follow [these instructions](https://withoutmyconsent.org/resources/take-down) to take down content from a website.

Has the content been taken down by the website?

- [Yes](#one_more_persons)
- [No](#harassment_end)

### hate_speech

Is the attack based on attributes like race, gender, or religion?

- [Yes](#one_more_persons)
- [No](#harassment_end)


### one_more_persons

Have you been attacked by one or more persons?

- [One person](#one_person)
- [More persons](#more_persons)

### one_person

Do you know this person?

- [Yes](#known_harasser)
- [No](#block_harasser)


### known_harasser

> If you know who is harassing you, you can think of reporting them to your country's authorities. Each country has different laws for protecting people from online harassment, and you should explore the legislation in your country to decide what to do.
>
> If you decide to sue this person, you should reach out to a legal expert.

Do you want to sue the attacker?

 - [Yes](#legal_end)
 - [No](#block_harasser)


### block_harasser

> Whether you know who your harasser is or not, it's always a good idea to block them on social networking platforms whenever possible.
>
> - [Facebook](https://www.facebook.com/help/290450221052800)
> - [Twitter](https://help.twitter.com/en/using-twitter/blocking-and-unblocking-accounts)
> - [Google](https://support.google.com/accounts/answer/6388749?co=GENIE.Platform%3DDesktop&hl=en)
> - [Tumblr](https://tumblr.zendesk.com/hc/en-us/articles/231877648-Blocking-users)
> - [Instagram](https://help.instagram.com/426700567389543)

Have you blocked your harasser effectively?

 - [Yes](#resolved_end)
 - [No](#harassment_end)


### more_persons

> If you are being attacked by more than one person, you might be the target of a harassment campaign, and you will need to reflect on what is the best strategy that applies to your case.
>
> To learn about all the possible strategies, read this [page](https://www.takebackthetech.net/be-safe/hate-speech-strategies)

Have you identified the best strategy for you?

 - [Yes](#resolved_end)
 - [No](#harassment_end)

### harassment_end

> If you are still under harassment and need a customized solution, please contact the organizations below who can support you.

:[](organisations?services=harassment)


### physical_risk_end

> If you are at physical risk, please contact the organizations below who can support you.

:[](organisations?services=physical_security)


### legal_end

> If you need legal support, please contact the organizations below who can support you.

:[](organisations?services=legal)

### resolved_end

We hope this troubleshooting guide was useful. Please give us feedback [via email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- **Document harassment:** It is helpful to document the attacks or any other incident you may be witnessing: take screenshots, save the messages you receive from harassers, etc. If possible, create a journal where you can systematize this documentation recording dates, times, platforms and on line places, user ID, screenshots, description of what happened, etc. Journals can help you detect possible patterns and indications about your possible attackers. If you feel overwhelmed, try to think of someone you trust who could document the incidents for you for a while. You should trust deeply the person who will manage this documentation, as you will need to hand them over credentials to your personal accounts. Once you feel you can regain control of their account, remember to change your passwords.

    - You can find instructions on how to document the incident in [this page](https://www.techsafety.org/documentationtips/).

- **Set up 2-factor authentication** on all your accounts. 2-factor authentication can be very effective for stopping someone from accessing your accounts without your permission. If you can choose, don't use SMS-based 2-factor authentication and choose a different option, based on a phone app or on a security key.

    - If you don't know what solution is best for you, you can check out [this infographic](https://www.accessnow.org/cms/assets/uploads/2017/09/Choose-the-Best-MFA-for-you.png) and [this post](https://www.eff.org/deeplinks/2017/09/guide-common-types-two-factor-authentication-web).
    - You can find instructions for setting up 2-factor authentication on the major platforms [here](https://www.eff.org/deeplinks/2016/12/12-days-2fa-how-enable-two-factor-authentication-your-online-accounts).

- **Map your online presence**. Self-doxing consists in exploring open source intelligence on oneself to prevent malicious actors from finding and using this information for impersonating you.


#### Resources

- [What to Do if You're Being Doxed](https://www.wired.com/story/what-do-to-if-you-are-being-doxed/)
- [Locking Down Your Digital Identity](http://femtechnet.org/csov/lock-down-your-digital-identity/)
- [Anti-doxing Guide for Activists Facing Attacks from the Alt-Right](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)
- [A guide to prevent doxing](https://guides.accessnow.org/self-doxing/self-doxing.html)
