---
layout: topic
title: "My device is acting suspiciously"
author: Donncha Ó Cearbhaill, Claudio Guarnieri, Rarenet
language: pt
summary: "If your computer or phone is acting suspiciously, there may be unwanted or malicious software on your device."
date: 2019-03
permalink: /pt/topics/device-acting-suspiciously
parent: /pt/
---

# My device is acting suspiciously

Malware attacks have evolved and become highly sophisticated over the years. These attacks pose multiple different threats and can have serious implications to your personal and organisational infrastructure and data.

Malware attacks come in different forms, such as viruses, phishing, ransomware, trojans and rootkits. Some of the threats are: computers crashing, data theft (i.e.: sensitive account credentials, financial info, bank account logins), an attacker blackmailing you to pay a ransom by taking control of your device, or taking control of your device and using it to launch DDoS attacks.

Some methods commonly used by attackers to compromise you and your devices seem like regular activities, such as:

- An email or a post on social media that will tempt you to open an attachment or click on a link.

- Pushing people to download and install software from an untrusted source.

- Pushing someone to enter their username and password into a website that is made to look legitimate, but is not.

This section of the Digital First Aid Kit will walk you through some basic steps to figure out if your device is likely infected or not.

If you think that your computer or mobile device has started acting suspiciously, you should first think of what the symptoms are.

Symptoms that commonly can be read as suspicious device activity, but often are not sufficient reason to worry include:

- Clicking noises during phone calls
- Unexpected battery drain
- Overheating while the device is not in use
- A device operating slowly

These symptoms are often misconceived as reliable indicators of worrisome device activity. However any of them taken on its own is not sufficient reason for concern.

Reliable symptoms of a compromised device usually are:

- The device restarts frequently on its own
- Applications crash, especially after input action
- Operating system updates and/or security patches fail repeatedly
- Webcam activity indicator light is on while webcam is not in use
- Repeated ["Blue Screens of Death"](https://en.wikipedia.org/wiki/Blue_Screen_of_Death) or kernel panics
- Flashing windows
- Antivirus warnings

## Workflow

### start

Given the information provided in the introduction, if you still feel that your device may be compromised the following guide may help you identify the problem.

 - [I believe my mobile device is acting suspiciously](#phone-intro)
 - [I believe my computer is acting suspiciously](#computer-intro)
 - [I no longer feel that my device may be compromised](#device-clean)


### device-clean

> Great! However, bear in mind that these instructions will help you to perform only a quick analysis. While it should be sufficient to identify visible anomalies, more sophisticated spyware could be capable of hiding more effectively.

If you still suspect that the device might be compromised you may want to:

- [seek for additional help](#malware_end)
- [proceed directly to a reset of the device](#reset).


### phone-intro

> It is important to consider how your device may have become compromised.
>
> - How long ago did you start suspecting that your device was acting suspiciously?
> - Do you remember clicking on any links from unknown sources?
> - Have you received messages from parties that you do not recognize?
> - Did you install any unsigned software?
> - Has the device been out of your possession?

Reflect on these questions to try and identify the circumstances, if any, that led to your device being compromised.

 - [I have an Android device](#android-intro)
 - [I have an iOS device](#ios-intro)


### android-intro

> First check if there are any unfamiliar apps installed on your Android device.
>
> You can find a list of apps in the "Apps" section of the settings menu. Identify any apps that did not come pre-installed with your device and that you do not remember downloading.
>
> If you suspect any of the apps in the list, run a web search and look for resources to see if there are any reports that identify the app as malicious.

Did you find any suspicious apps?

 - [No, I didn't](#android-unsafe-settings)
 - [Yes, I identified potentially malicious apps](#android-badend)


### android-unsafe-settings

> Android gives users the option to enable lower-level access to their device. This can be useful for software developers, but it can also expose devices to additional attacks. You should review these security settings and ensure they are set to the safer options. Manufactures may ship devices with insecure defaults. These settings should be reviewed even if you haven't made changes yourself.
>
> #### Unsigned Applications
>
> Android normally blocks the installation of apps which are not loaded from the Google Play Store. Google has processes to review and identify malicious apps on the Play Store. Attackers often try to avoid these checks by serving malicious apps directly to the user by sharing a link or a file with them. It is important to ensure your device does not allow the installation of applications from untrusted sources.
>
> Go to the "Security" section of your Android settings and make sure that the that option Install application from Unknown Source is disabled.
>
> #### Developer Mode and ADB access (Android Debug Bridge)
>
> Android allows developers to directly run commands on the underlying operating system while in "Developer Mode". When enabled, this exposes the devices to physical attacks. Someone with physical access to the device  could use the Developer mode to download copies of private data from the device or to install malicious applications.
>
> If you see a Developer mode menu in your device settings then you should ensure that ADB access is disabled.
>
> #### Google Play Protect
>
> The Google Play Protect service is available on all recent Android devices. It performs regular scans of all applications installed on your device. Play Protect can also automatically remove any known malicious applications from your device. Enabling this service does send information about your device (such installed applications) to Google.
>
> Google Play Protect can be enabled under your device security settings. More information is available in the [Play Protect](https://www.android.com/play-protect/) site.

Did you identify any insecure settings?

- [No, I didn't](#android-bootloader)
- [Yes, I identified potentially insecure settings](#android-badend)


### android-bootloader

> The Android bootloader is a key software that runs as soon as you turn on your device. The bootloader enables the operating system to start and use the hardware. A compromised bootloader gives an attacker full access to the device hardware. The majority of manufactures ship their devices with a locked bootloader. A common way to identify if the manufacturer's signed bootloader has been changed is to restart your device and look for the boot logo. If a yellow triangle with an exclamation mark is appears, then the original bootloader has been replaced. Your device may also be compromised if it shows an unlocked bootloader warning screen and you haven't unlocked it yourself to install a custom Android ROM such as CyanogenMod. You should perform a factory reset of your device if it shows an unlocked bootloader warning screen that you don't expect.

Is the bootloader compromised or is your device using the original bootloader?

- [The bootloader on my device is compromised](#android-badend)
- [My device is using the original bootloader](#android-goodend)


### android-goodend

> It does not seem like your device has been compromised.

Are you still worried that your device is compromised?

- [Yes, I would like to seek professional help](#malware_end)
- [No, I've solved my issues](#resolved_end)


### android-badend

> Your device may be compromised. A factory [reset](#reset) will likely remove any threat that is present on your device. However, it is not always the best solution. In addition, you may want to investigate the matter further to identify your level of exposure and the precise nature of the attack that you endured.
>
> You might want to use a tool for self-diagnosis called [Emergency VPN](https://www.civilsphereproject.org/emergency-vpn), or seek assistance from an organization that can help.

Would you like to seek further assistance?

- [Yes, I would like to seek professional help](#malware_end)
- [No, I have a local support network I can reach out to](#resolved_end)


### ios-intro

> Check the iOS settings to see if there is anything unusual.
>
> In the Settings app, check that your device is tied to your Apple ID. The first menu item on the left-hand-side should be your name or the name you use for your Apple ID. Click on it and check that the correct email address is shown. At the bottom of this page you will see a list with the names and models of all iOS devices tied to this Apple ID.

 - [All the information is correct and I am still in control of my Apple ID](#ios-goodend)
 - [The name or others details are incorrect or I see devices in the list that are not mine](#ios-badend)


### ios-goodend

> It does not seem like your device has been compromised.

Are you still worried that your device is compromised?

- [Yes, I would like to seek professional help](#malware_end)
- [No, I've solved my issues](#resolved_end)


### ios-badend

> Your device may be compromised. A factory reset will likely remove any threat that is present on your device. However, it is not always the best solution. In addition, you may want to investigate the matter further to identify your level of exposure and the precise nature of the attack that you endured.
>
> You might want to use a tool for self-diagnosis called [Emergency VPN](https://www.civilsphereproject.org/emergency-vpn), or seek assistance from an organization that can help.

Would you like to seek further assistance?

- [Yes, I would like to seek professional help](#malware_end)
- [No, I have a local support network I can reach out to](#resolved_end)


### computer-intro

> **Note: In the case that you are under a ransomware attack head directly to [this website](https://www.nomoreransom.org/).**
>
> This workflow will help you investigate suspicious activity on your computer device. If you are assisting a person remotely you can try to follow the steps described in the links below using a remote desktop software like TeamViewer, or you can look into remote forensic frameworks such as [Google Rapid Response (GRR)](https://github.com/google/grr). Bear in mind that network latency and reliability will be essential to do this properly.

Please select your operating system:

 - [I have a Windows computer](#windows-intro)
 - [I have a Mac computer](#mac-intro)


### windows-intro

> You can follow this introductory guide for investigating suspicious activity on Windows devices:
>
> - [How to Live Forensic on Windows by Tek](https://github.com/Te-k/how-to-quick-forensic/blob/master/Windows.md)

Did these instructions help you identify any malicious activity?

 - [Yes, I think the computer is infected](#device-infected)
 - [No, no malicious activity was identified](#device-clean)


### mac-intro

> To identify a potential infection on a Mac computer, you should follow these steps:
>
> 1. Check for suspicious programs starting automatically
> 2. Check for suspicious running processes
> 3. Check for suspicious kernel extensions
>
> The website [Objective-See](https://objective-see.com) provides several freeware utilities that facilitate this process:
>
> - [KnockKnock](https://objective-see.com/products/knockknock.html) can be used to identify all programs that are registered to start automatically.
> - [TaskExplorer](https://objective-see.com/products/taskexplorer.html) can be used to check running processes and identify those that look suspicious (for example because not signed, or because they are flagged by VirusTotal).
> - [KextViewr](https://objective-see.com/products/kextviewr.html) can be used to identify any suspicious kernel extension that is loaded on the Mac computer.
>
> In case these do not reveal anything immediately suspicious and you want to perform further triaging you could use [Snoopdigg](https://github.com/botherder/snoopdigg). Snoopdigg is a utility that simplifies the process of collecting some information on the system and take a full memory snapshot.
>
> An additional tool that could be useful to collect further details (but that requires some familiarity with terminal commands) is [AutoMacTC](https://www.crowdstrike.com/blog/automating-mac-forensic-triage/) by the American cybersecurity company CrowdStrike.

Did these instructions help you identify any malicious activity?

 - [Yes, I think the computer is infected](#device-infected)
 - [No, no malicious activity was identified](#device-clean)

### device-infected

Oh no! To get rid of the infection you might want to:

- [seek additional help](#malware_end)
- [proceed directly to a reset of the device](#reset).

### reset

> You may want to consider resetting your device as an extra cautionary measure. The guides below will provide appropriate instructions for your type of device:
>
> - [Android](https://www.howtogeek.com/248127/how-to-wipe-your-android-device-and-restore-it-to-factory-settings/)
> - [iOS](https://support.apple.com/en-us/HT201252)
> - [Windows](https://support.microsoft.com/en-us/help/4000735/windows-10-reinstall)
> - [Mac](https://support.apple.com/en-us/HT201314)

Do you feel you need additional help?

- [Yes](#malware_end)
- [No](#resolved_end)


### malware_end

If you need additional help in dealing with an infected device, you can reach out to the organizations listed below.

:[](organisations?services=vulnerabilities_malware)

### resolved_end

We hope the DFAK was useful. Please give us feedback [via email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Here are some tips to prevent yourself from falling prey to an attacker's efforts to compromise your devices and data:

- Always double-check the legitimacy of any email you receive, a file you have downloaded or a link asking you for your account login details.
- Read more about how to protect your device from malware infections in the guides linked in the resources.

#### Resources

- [Security in a Box - Avoiding Malware and Phishing Attacks"](https://securityinabox.org/en/guide/malware/#avoiding-malware-and-phishing-attacks)
- [Security in a Box - Protect your device from malware and phishing attacks](https://securityinabox.org/en/guide/malware/)
