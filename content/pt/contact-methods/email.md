---
layout: page
title: Email
author: mfc
language: pt
summary: Contact methods
date: 2018-09
permalink: /pt/contact-methods/email.md
parent: /pt/
published: true
---

The content of your message as well as the fact that you contacted the organization may be accessible by governments or law enforcement agencies.