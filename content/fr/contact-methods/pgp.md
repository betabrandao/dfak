---
layout: page
title: PGP
author: mfc
language: fr
summary: Contact methods
date: 2018-09
permalink: /fr/contact-methods/pgp.md
parent: /fr/
published: true
---

PGP (or Pretty Good Privacy) and its open source equivalent, GPG (Gnu Privacy Guard), allows you to encrypt the contents of emails to protect your message from viewing by your email provider or any other party who may have access to the email. However the fact that you have sent a message to the recipient organization may be accessible by governments or law enforcement agencies. To prevent this, you could create an alternative email address not associated with your identity.

Resources: [Resource on creating alternative email addresses](),[Encrypting Email with Mailvelope: A Beginner's Guide](https://freedom.press/training/encrypting-email-mailvelope-beginners-guide/)