---
layout: page
title: Tor
author: mfc
language: fr
summary: Contact methods
date: 2018-09
permalink: /fr/contact-methods/tor.md
parent: /fr/
published: true
---

The Tor Browser is a privacy-focused web browser that enables you to interact with websites anonymously by not sharing your location (via your IP address) when you access the website. Resources: 
[Overview of Tor](https://www.torproject.org/about/overview.html.en).